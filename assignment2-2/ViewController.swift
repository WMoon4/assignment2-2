//
//  ViewController.swift
//  assignment2-2
//
//  Created by V.K. on 2/6/20.
//  Copyright © 2020 V.K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            
        // #1
        calculateCurrentValue()
            
        // #2
        calculateUpfrontReserve()
        
        // #3
        calculateMonthCovered()
        
        // #4*
        reversedNumber(number: 1020304)
            
    }
    
    func calculateCurrentValue() {
        print("---------- 1 ----------")
        let initialAmount = 24.0
        let yearOne = 1826
        let yearToday = 2020
        let rate = 0.06
        
        var currAmount = initialAmount
        for _ in yearOne+1...yearToday {
            currAmount *= 1 + rate
        }
        print("deposit, annual capitalization, " + String(format: "%.1f%% per annum", rate * 100))
        print(String(format: "initial amount $%.2f", initialAmount) + " on year of \(yearOne)")
        print("year of \(yearToday) withdrawal: $" + String(format: "%.2f", currAmount))
    }
    
    func calculateUpfrontReserve() {
        print("---------- 2 ----------")
        let monthlyPay = 700.0
        let period: Int = 10
        let rate = 0.03
        
        var balance: Double = 0
        var monthlyCosts = 1000.0
        //
        for _ in 1...period {
            // end of month balance
            balance += monthlyPay - monthlyCosts
            monthlyCosts *= 1 + rate
        }
        print(String(format: "Upfront amount needed UAH%.2f", -balance))
    }
    
    func calculateMonthCovered() {
        print("---------- 3 ----------")
        let savings = 2400.0
        let monthlyPay = 700.0
        let rate = 0.03
        
        var monthlyCosts = 1000.0
        var balance = savings
        var monthCounter = 0
        //
        repeat {
            balance += monthlyPay - monthlyCosts
            monthlyCosts *= 1 + rate
            monthCounter += 1
        } while balance >= 0
        print("Going short at the end of month \(monthCounter)")
    }
    
    func reversedNumber(number: Int) {
        print("---------- 4* ----------")
        print("Input number is \(number)")
        assert(number > 0, "Invalid number: should be integer above 0.")
        
        var currNumber: Int = number
        var result: Int = 0
        while currNumber != 0 {
            result = result * 10 + currNumber % 10
            currNumber /= 10
        }
        print("Reversed number is \(result)")
        
    }


}

